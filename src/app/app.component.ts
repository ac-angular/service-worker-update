import { Component } from '@angular/core';
import { VersionUpdateService } from './services/version-update.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'arnaldo-ceballos';

  public constructor(private versionUpdateService: VersionUpdateService) {}
}
