import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { ExperiencesComponent } from './experiences/experiences.component';
import { ContactComponent } from './contact/contact.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { WorksComponent } from './works/works.component';
import { VersionUpdateInterceptor } from './interceptors/version-update.interceptor';
import {MatDialogModule} from '@angular/material/dialog';
import { UpdateNotificationComponent } from './update-notification/update-notification.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ExperiencesComponent,
    ContactComponent,
    WorksComponent,
    UpdateNotificationComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatDialogModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:3000'
    }),
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [ {
    provide: HTTP_INTERCEPTORS,
    useClass: VersionUpdateInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
