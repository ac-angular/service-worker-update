import { TestBed } from '@angular/core/testing';

import { VersionUpdateInterceptor } from './version-update.interceptor';

describe('VersionUpdateInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      VersionUpdateInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: VersionUpdateInterceptor = TestBed.inject(VersionUpdateInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
