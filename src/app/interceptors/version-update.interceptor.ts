import { ApplicationRef, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { concat, interval, Observable } from 'rxjs';
import { SwUpdate } from '@angular/service-worker';
import { first } from 'rxjs/operators';

@Injectable()
export class VersionUpdateInterceptor implements HttpInterceptor {

  constructor(private app: ApplicationRef,private updates: SwUpdate) {
      //     // 1. Inicialización
      //     this.updates.available.subscribe((event) => {
      //       console.log("quiere confirmar");
      //       if (confirm('Hay una nueva versión de la aplicación. ¿Deseas instalarla ahora?')) {
      //         this.updates.activateUpdate().then(() =>  window.location.reload());
      //       }
      //     });    
      //           // 3. Inconsistencias
      // this.updates.unrecoverable.subscribe((event) => {
      //   alert('Se ha producido un error y no podemos cargar la aplicación. Por favor, recarga la página para solucionarlo.');
      // });

  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // console.log("entro al interceptor");
    // if (this.updates.isEnabled) {
      
    //   // 2. Comprobación
    //   const appIsStable$ = this.app.isStable.pipe(
    //     first((isStable) => isStable === true)
    //   );
    //   const checkInterval$ = interval(0);
    //   const everyIntervalOnceAppIsStable$ = concat(
    //     appIsStable$,
    //     checkInterval$
    //   );
    //   everyIntervalOnceAppIsStable$.subscribe(() => this.updates.checkForUpdate());
    //   console.log("termina check");
    //   }
    //   console.log("retorna");
      return next.handle(request);  
    }
}
