import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VersionUpdateService {
  constructor(app: ApplicationRef, updates: SwUpdate) {
    console.log("updates.isEnabled", updates.isEnabled);
    if (updates.isEnabled) {
      
      // 1. Inicialización
      updates.available.subscribe((event) => {
        if (confirm('Hay una nueva versión de la aplicación. ¿Deseas instalarla ahora?')) {
          updates.activateUpdate().then(() =>  window.location.reload());
        }
      });

      // 2. Comprobación
      const appIsStable$ = app.isStable.pipe(
        first((isStable) => isStable === true)
      );
      const checkInterval$ = interval(5 * 1000);
      const everyIntervalOnceAppIsStable$ = concat(
        appIsStable$,
        checkInterval$
      );
      everyIntervalOnceAppIsStable$.subscribe(() => updates.checkForUpdate());

      // 3. Inconsistencias
      updates.unrecoverable.subscribe((event) => {
        alert('Se ha producido un error y no podemos cargar la aplicación. Por favor, recarga la página para solucionarlo.');
      });
    }
  }  
}
